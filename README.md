
# Dumper

Dumper is a helper to dump data from a RDBMS into a file in a non-sql
format like XML or JSON. Dumper allows you to query a database and transform
the fetched rows into an appropriate format.

![Dumper truck](dumper.jpg "Dumper truck")

## Quick features list

- Supported formats out of the box: JSON, XML and CSV
- Partial saves and fetches, don't load all records into memory
- Uses [PDO][pdo], so many RDBMS [drivers available][pdo-drivers]
- Minimal dependencies, only PHP extensions: PDO, XML and JSON
- Output and data structure easily customizable

[pdo]: http://php.net/manual/en/book.pdo.php
[pdo-drivers]: http://php.net/manual/en/pdo.drivers.php


## Example of usage

Define your settings, write a SQL query for the data you want to fetch, and
then you can either define a transformer to customize the data structure or
just use the default transformer.

```php
$dumper = new Dumper([
    'pprint' => true, // Default false
    'outputFormat' => 'xml', // Default format
    'limit' => 500, // Default 1000
    'verbose' => false, // Default false
    'xmlEncoding' => 'utf-8', // Default value
    'xmlVersion' => '1.0', // Default value
    'xmlRootElement' => 'languages', // Default value 'items'
    'csvDelimiter' => ',', // Default value
    'pdo' => [ // A PDO instance is also possible
        // @see http://php.net/manual/en/pdo.construct.php
        'dsn' => 'sqlite:/tmp/test.sqlite3',
        'username' => 'dbuser',
        'password' => 'dbpass',
        'options' => [], // Default value
    ],
]);

$dumps = [
    '/tmp/test1.xml' => [
        'query' => 'SELECT id, name, iso1, variants FROM language',
        'tranformer' => function ($row) {
            return [
                '@parent' => 'language',
                '%code' => $row['iso1'],
                'name' => $row['name'],
                'variants' => array_map(
                    function ($lang) {
                        return ['variant' => $lang];
                    },
                    array_slice(explode(',', $row['variants']), 0, 2)
                )
            ];
        },
    ],
    '/tmp/test2.csv' => [
        'query' => 'SELECT id, name, iso2 AS iso_code FROM language',
        'count' => 'SELECT COUNT(*) FROM language',
    ],
];

foreach ($dumps as $file => $seed) {
    $format = pathinfo($file, PATHINFO_EXTENSION);
    $dumper->setOutputFormat($format)
        ->setFetchQuery($seed['query'])
        // setCountQuery is optional, but in some cases could be necessary
        // when a SELECT query is far too complex and Dumper cannot extract a
        // valid COUNT query from it
        ->setCountQuery($seed['count'] ?? null)
        ->setTransformer($seed['transformer'] ?? null)
        ->dump($file);
}
```

## Visual example

Here you can visualize what the example code above would produce. Given the
following data table:

id | name | iso1 | variants
---|------|------|---------
1 | German | de | ch,at
2 | French | fr | lu,be,ca

The first dump would produce following XML output:

```xml
<?xml version="1.0" encoding="utf-8" ?>
<languages>
    <language>
        <name code="de">German</name>
        <variants>
            <variant>ch</variant>
            <variant>at</variant>
        </variants>
    </language>
    <language>
        <name code="fr">French</name>
        <variants>
            <variant>lu</variant>
            <variant>be</variant>
            <variant>ca</variant>
        </variants>
    </language>
</languages>
```

The second dump would produce following CSV output:

```csv
"id","name","iso_code"
"1","German","de"
"2","French","fr"
```

Checkout [unit tests][tests] for more examples and the class [Xmler][xmler]
for more details about how an array is converted to a XML.

[tests]: ./test/
[xmler]: ./src/Dumper/Xmler.php
