<?php

namespace Dumper\Test;

// Extends
use PHPUnit\Framework\TestCase;

// Uses
use Dumper\Xmler;

/**
 * Class XmlerTest
 */
class XmlerTest extends TestCase
{
    /**
     * @return void
     */
    public function testCreateDocument(): void
    {
        $items = [
            ['country' => ['name' => 'Germany']],
            ['country' => ['name' => 'Denmark']],
            ['country' => ['name' => 'Austria']],
            ['country' => ['name' => 'Finland']],
            ['country' => ['name' => 'France']],
        ];

        $xml = new Xmler();
        $output = $xml->make($items, 'countries');

        $xmlOut = new \DOMDocument;
        $xmlOut->loadXML($output);

        $xmlSrc = new \DOMDocument;
        $xmlSrc->loadXML('<?xml version="1.0" encoding="utf-8"?>
            <countries>
                <country>
                    <name>Germany</name>
                </country>
                <country>
                    <name>Denmark</name>
                </country>
                <country>
                    <name>Austria</name>
                </country>
                <country>
                    <name>Finland</name>
                </country>
                <country>
                    <name>France</name>
                </country>
            </countries>
        ');

        $this->assertEqualXMLStructure(
            $xmlSrc->firstChild,
            $xmlOut->firstChild
        );
    }

    /**
     * @return void
     */
    public function testCreateDocumentWithAttributes(): void
    {
        $items = [
            [
                'country' => [
                    '@isoCode' => 'DE',
                    'name' => 'Germany',
                    'capital' => [
                        'name' => 'Berlin',
                        'population' => 3470000,
                    ],
                ],
            ],
            [
                'country' => [
                    '@isoCode' => 'FI',
                    'name' => 'Finland',
                    'captial' => [
                        'name' => 'Helsinki',
                        'population' => 608690,
                    ]
                ],
            ]
        ];

        $xml = new Xmler(null, null);
        $output = $xml->make($items, 'countries');

        $xmlOut = new \DOMDocument;
        $xmlOut->loadXML($output);

        $xmlSrc = new \DOMDocument;
        $xmlSrc->loadXML('
            <countries>
                <country isoCode="DE">
                    <name>Germany</name>
                    <capital>
                        <name>Berlin</name>
                        <population>3470000</population>
                    </capital>
                </country>
                <country isoCode="FI">
                    <name>Finland</name>
                    <captial>
                        <name>Helsinki</name>
                        <population>608690</population>
                    </captial>
                </country>
            </countries>
        ');

        $this->assertEqualXMLStructure(
            $xmlSrc->firstChild,
            $xmlOut->firstChild,
            true
        );
    }

    /**
     * @return void
     */
    public function testCreateDocumentWithADeclarativeParent(): void
    {
        $items = [
            ['%parent' => 'country', 'name' => 'Germany'],
            ['%parent' => 'country', 'name' => 'Denmark'],
            ['%parent' => 'country', 'name' => 'Austria'],
            ['%parent' => 'country', 'name' => 'Finland'],
            ['%parent' => 'country', 'name' => 'France'],
        ];

        $xml = new Xmler();
        $output = $xml->make($items, 'countries');

        $xmlOut = new \DOMDocument;
        $xmlOut->loadXML($output);

        $xmlSrc = new \DOMDocument;
        $xmlSrc->loadXML('<?xml version="1.0" encoding="utf-8"?>
            <countries>
                <country>
                    <name>Germany</name>
                </country>
                <country>
                    <name>Denmark</name>
                </country>
                <country>
                    <name>Austria</name>
                </country>
                <country>
                    <name>Finland</name>
                </country>
                <country>
                    <name>France</name>
                </country>
            </countries>
        ');

        $this->assertEqualXMLStructure(
            $xmlSrc->firstChild,
            $xmlOut->firstChild
        );
    }

    /**
     * @expectedException UnexpectedValueException
     * @expectedExceptionMessageRegExp 'Parent already declared'
     * @return void
     */
    public function testThrowExceptionOnMultipleParentDeclaration(): void
    {
        $items = [
            ['%parent' => 'country', '%land' => '', 'name' => 'Germany'],
            ['%parent' => 'country', 'name' => 'Denmark'],
        ];

        $xml = new Xmler();
        $xml->make($items, 'countries');
    }

    /**
     * @expectedException UnexpectedValueException
     * @expectedExceptionMessage Unexpected type "array", string expected
     * @return void
     */
    public function testThrowExceptionOnInvalidDataTypeForAttribute(): void
    {
        $items = [
            'country' => [
                '@isoCode' => ['de', 'de_DE'],
                'name' => 'Germany',
            ]
        ];

        $xml = new Xmler();
        $xml->make($items, 'countries');
    }

    /**
     * @return void
     */
    public function testCreateDocumentThatContainsSpecialSymbols(): void
    {
        $xml = new Xmler();

        // --

        $items = [
            ['html' => '<span style="display:block">Name</span>'],
        ];

        $output = $xml->make($items, 'document');

        $xmlOut = new \DOMDocument;
        $xmlOut->loadXML($output);

        $xmlSrc = new \DOMDocument;
        $xmlSrc->loadXML(
            '<document>' .
                '<html>' .
                    '&lt;span style=&quot;display:block&quot;&gt;' .
                       'Name' .
                    '&lt;/span&gt;' .
                '</html>' .
            '</document>'
        );

        $this->assertEqualXMLStructure(
            $xmlSrc->firstChild,
            $xmlOut->firstChild
        );

        $this->assertXmlStringEqualsXmlString(
            $xmlSrc->saveXML(),
            $xmlOut->saveXML()
        );

        // --

        $items = [
            ['xml' => "<xml>Divide & conquer aka. ('Divide and rule')</xml>)"],
        ];

        $output = $xml->make($items, 'document');

        $xmlOut = new \DOMDocument;
        $xmlOut->formatOutput = false;
        $xmlOut->loadXML($output);

        $xmlSrc = new \DOMDocument;
        $xmlSrc->formatOutput = false;
        $xmlSrc->loadXML(
            '<document>' .
                '<xml>' .
                    '&lt;xml&gt;' .
                        'Divide &amp; conquer aka. (\'Divide and rule\')' .
                    '&lt;/xml&gt;)' .
                '</xml>' .
            '</document>'
        );

        $this->assertEqualXMLStructure(
            $xmlSrc->firstChild,
            $xmlOut->firstChild
        );

        $this->assertXmlStringEqualsXmlString(
            $xmlSrc->saveXML(),
            $xmlOut->saveXML()
        );
    }
}
