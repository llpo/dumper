<?php

namespace Dumper\Test;

// Extends
use PHPUnit\Framework\TestCase;

// Uses
use Dumper\Dumper;
use Dumper\Xmler;
use Faker\Factory;

/**
 * Class DumperTest
 */
class DumperTest extends TestCase
{
    /**
     * @var string
     */
    const DATABASE = 'sqlite::memory:';

    /**
     * @var int
     */
    const NUM_ROWS = 50;

    /**
     * @var \PDO
     */
    protected static $connection;

    /**
     * @return void
     */
    public static function setUpbeforeClass(): void
    {
        self::$connection = new \PDO(self::DATABASE);
        self::$connection->setAttribute(
            \PDO::ATTR_ERRMODE,
            \PDO::ERRMODE_EXCEPTION
        );

        self::$connection->exec('
            CREATE TABLE IF NOT EXISTS polyglot(
                id INTEGER PRIMARY KEY,
                name CHAR(80) NOT NULL,
                email CHAR(256) NOT NULL,
                city CHAR(128),
                languages TEXT
            );
        ');

        $faker = Factory::create();

        $languages = [];
        $validator = function ($languageCode) use (&$languages) {
            return !in_array($languageCode, $languages);
        };

        foreach (range(1, self::NUM_ROWS) as $id) {
            $languages = [];
            for ($x = 0; $x < rand(2, 5); $x++) {
                $languages[] = $faker->valid($validator)->languageCode;
            }
            $params = [
                'id' => $id,
                'name' => $faker->name,
                'email' => $faker->email,
                'city' => $faker->city,
                'languages' => implode(',', $languages)
            ];
            $statement = self::$connection->prepare('
                INSERT INTO polyglot (id, name, email, city, languages)
                VALUES (:id, :name, :email, :city, :languages);
            ');
            $statement->execute($params);
        }

        $statement = self::$connection->query(
            'SELECT COUNT(*) AS count FROM polyglot'
        );

        $row = $statement->fetch(\PDO::FETCH_ASSOC);
        if ($row['count'] != self::NUM_ROWS) {
            throw new \RuntimeException('No records were saved');
        }
    }

    /**
     * @param string $filename e.g. test-file.json
     * @return string e.g. /tmp/test-file.json
     */
    protected function getFullFilePath(string $filename): string
    {
        return "/tmp/$filename";
    }

    /**
     * @return void
     */
    public function testDumpXmlUsingDefaultTransformer(): void
    {
        $statement = self::$connection->prepare('
            UPDATE polyglot SET
                name = :name,
                email = :email,
                city = :city,
                languages = :languages
            WHERE id = 50
        ');

        $me = [
            ':name' => 'Toni Pons',
            ':email' => 'me@fake.org',
            ':city' => 'Berlin',
            ':languages' => 'ca,es,de,en',
        ];

        $statement->execute($me);

        $dumper = new Dumper([
            'limit' => 20,
            'pprint' => true,
            'outputFormat' => 'xml',
            'pdo' => self::$connection,
            'xmlRootElement' => 'polyglots',
        ]);

        $dumper->setFetchQuery(
            'SELECT \'polyglot\' AS \'%parent\', * FROM polyglot'
        );

        $outputFile = $this->getFullFilePath('test-default-transformer.xml');

        $this->assertEquals(self::NUM_ROWS, $dumper->dump($outputFile, true));

        $xml = new \DOMDocument;
        $xml->loadXML(file_get_contents($outputFile));

        $xpath = new \DOMXPath($xml);

        $query = "count(//polyglots/polyglot/id[text() > 0])";
        $this->assertEquals(self::NUM_ROWS, $xpath->evaluate($query));

        $result = $xpath->evaluate("//polyglots/polyglot/id[text() = 50]/..");
        $this->assertEquals(1, $result->length);

        foreach ($me as $prop => $value) {
            $tagName = substr($prop, 1);
            $nodeValue = $result[0]->getElementsByTagName($tagName)[0]->nodeValue;
            $this->assertEquals($value, $nodeValue);
        }
    }

    /**
     * @return void
     */
    public function testDumpXmlUsingCustomTransformer(): void
    {
        $dumper = new Dumper([
            'pprint' => true,
            'outputFormat' => 'xml',
            'pdo' => self::$connection,
            'xmlRootElement' => 'polyglots',
        ]);

        $dumper->setFetchQuery('
                SELECT \'polyglot\' AS \'%parent\', id, name, languages
                FROM polyglot WHERE id < 3;
            ')
            ->setTransformer(function ($row) {
                $row['languages'] = array_map(
                    function ($lang) {
                        return ['language' => $lang];
                    },
                    array_slice(explode(',', $row['languages']), 0, 2)
                );
                return $row;
            });

        $outputFile = $this->getFullFilePath('test-custom-transformer.xml');

        $this->assertEquals(2, $dumper->dump($outputFile, true));

        $xmlOut = new \DOMDocument;
        $xmlOut->loadXML(file_get_contents($outputFile));

        $xmlSrc = new \DOMDocument;
        $xmlSrc->loadXML(
            '<?xml version="1.0" encoding="utf-8" ?>' .
            '<polyglots>' .
                '<polyglot>' .
                    '<id>1</id>' .
                    '<name>Miss Lexie Luettgen</name>' .
                    '<languages>' .
                        '<language>bg</language>' .
                        '<language>ro</language>' .
                    '</languages>' .
                '</polyglot>' .
                '<polyglot>' .
                    '<id>2</id>' .
                    '<name>Herman Kuhlman</name>' .
                    '<languages>' .
                        '<language>mg</language>' .
                        '<language>kr</language>' .
                    '</languages>' .
                '</polyglot>' .
            '</polyglots>'
        );

        $this->assertEqualXMLStructure(
            $xmlSrc->firstChild,
            $xmlOut->firstChild
        );
    }

    /**
     * @return void
     */
    public function testDumpJsonUsingDefaultTransformer(): void
    {
        $dumper = new Dumper([
            'limit' => 10,
            'pprint' => true,
            'outputFormat' => 'json',
            'pdo' => self::$connection,
        ]);

        $dumper->setFetchQuery('
            SELECT
            id, name AS fullname, email, city, languages
            FROM polyglot
        ');

        $outputFile = $this->getFullFilePath('test-default-transformer.json');

        $this->assertEquals(self::NUM_ROWS, $dumper->dump($outputFile, true));

        // JSON decode should fail
        json_decode(']');
        $this->assertNotEquals(json_last_error(), JSON_ERROR_NONE);

        // JSON decode works as expected, so now decoding shouldn't fail
        $buffer = file_get_contents($outputFile);
        $buffer = json_decode($buffer);
        $this->assertEquals(json_last_error(), JSON_ERROR_NONE, "Malformed JSON");

        $this->assertCount(self::NUM_ROWS, $buffer);
        $this->assertNotEmpty($buffer[0]->id);
        $this->assertNotEmpty($buffer[0]->fullname);
        $this->assertNotEmpty($buffer[0]->email);
        $this->assertNotEmpty($buffer[0]->city);
        $this->assertNotEmpty($buffer[0]->languages);
    }

    /**
     * @return void
     */
    public function testDumpJsonUsingCustomTransformer(): void
    {
        $dumper = new Dumper([
            'limit' => 10,
            'pprint' => true,
            'outputFormat' => 'json',
            'pdo' => self::$connection,
        ]);

        $dumper->setFetchQuery('
                SELECT
                id, name AS name, email, city, languages
                FROM polyglot
            ')
            ->setTransformer(function ($row) {
                $row['languages'] = explode(',', $row['languages']);
                return $row;
            });

        $outputFile = $this->getFullFilePath('test-custom-transformer.json');

        $this->assertEquals(self::NUM_ROWS, $dumper->dump($outputFile, true));

        $buffer = json_decode(file_get_contents($outputFile));
        $this->assertEquals(json_last_error(), JSON_ERROR_NONE, "Malformed JSON");
        $this->assertCount(self::NUM_ROWS, $buffer);
        $person = $buffer[0];
        $this->assertNotEmpty($person->id);
        $this->assertNotEmpty($person->name);
        $this->assertNotEmpty($person->email);
        $this->assertNotEmpty($person->city);
        $this->assertInternalType('array', $person->languages);
        $this->assertGreaterThan(2, $person->languages);
    }

    /**
     * @return void
     */
    public function testDumpCsvUsingDefaultTransformer(): void
    {
        $dumper = new Dumper([
            'pprint' => true,
            'outputFormat' => 'csv',
            'pdo' => self::$connection,
        ]);

        $dumper->setFetchQuery('SELECT id, email, languages FROM polyglot');

        $outputFile = $this->getFullFilePath('test-default-transformer.csv');

        $this->assertEquals(self::NUM_ROWS, $dumper->dump($outputFile, true));

        $buffer = file_get_contents($outputFile);
        $buffer = str_getcsv($buffer, PHP_EOL);
        $csv = array_map(
            function ($row) {
                return str_getcsv($row);
            },
            $buffer
        );

        // Number of rows including header
        $this->assertCount(self::NUM_ROWS + 1, $csv);

        // Number of columns
        $this->assertCount(3, $csv[0]);
        $this->assertCount(3, $csv[1]);
    }

    /**
     * @return void
     */
    public function testUseOfACustomCountQuery(): void
    {
        $dumper = new Dumper([
            'pprint' => true,
            'outputFormat' => 'csv',
            'pdo' => self::$connection,
        ]);

        $dumper->setFetchQuery('SELECT id FROM polyglot WHERE id <= 10');
        $dumper->setCountQuery('SELECT COUNT(*) FROM polyglot WHERE id <= 10');

        $outputFile = $this->getFullFilePath('test-custom-count-query.csv');

        $this->assertEquals(10, $dumper->dump($outputFile, true));
    }

    /**
     * @return void
     */
    public function testUseOfAWrongCustomCountQuery(): void
    {
        $dumper = new Dumper([
            'pprint' => true,
            'outputFormat' => 'csv',
            'pdo' => self::$connection,
        ]);

        $dumper->setFetchQuery('SELECT id FROM polyglot');
        $dumper->setCountQuery('SELECT COUNT(*), NULL as num FROM polyglot');

        $this->expectException(\UnexpectedValueException::class);
        $this->expectExceptionMessage(
            'COUNT query set with Dumper::setCountQuery must return only one'
        );
        $dumper->dump('this-file-should-never-exist.csv', true);
    }
}
