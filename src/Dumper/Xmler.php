<?php

namespace Dumper;

/**
 * Class Xmler
 *
 * Converts an array to XML. See {@see \Dumper\Xmler::make()} for more
 * details about the array format.
 */
class Xmler
{
    /**
     * @var string
     */
    private $version;

    /**
     * @var string
     */
    private $encoding;

    /**
     * @var bool
     */
    private $indent;

    /**
     * @param string $version XML Version, default 1.0
     * @param string $encoding XML Encoding, default UTF-8
     */
    public function __construct(
        ?string $version = '1.0',
        ?string $encoding = 'UTF-8',
        bool $indent = true
    )
    {
        $this->version = $version;
        $this->encoding = $encoding;
        $this->indent = $indent;
    }

    /**
     * Generartes an XML document from a given array.
     * ## From array:
     * [
     *      [
     *           'country' => [
     *              '@isoCode' => 'DE', // @ defines an attribute
     *              'name' => 'Germany',
     *              'capital' => [
     *                  'name' => 'Berlin',
     *                  'population' => 3470000,
     *               ],
     *           ],
     *      ],
     * ];
     * ## Following XML will be outputed:
     * <countries><!-- Root element -->
     *      <country isoCode="DE">
     *          <name>Germany</name>
     *          <capital>
     *              <name>Berlin</name>
     *              <population>3470000</population>
     *          </capital>
     *     </country>
     * </countries>
     *
     * You can use % to declare a parent tag, to see an example check
     * out {@see \Dumper\XmlerTest::testCreateDocumentWithADeclarativeParent()}
     *
     * @param array $items
     * @param string $rootElement
     * @throws \UnexpectedValueException
     * @return string XML
     */
    public function make(array $items, ?string $rootElement = null): string
    {
        $xml = new \XmlWriter();

        $xml->openMemory();
        $xml->setIndent($this->indent);
        $xml->setIndentString(str_repeat(' ', 4));

        if ($this->version !== null || $this->encoding !== null) {
            $xml->startDocument($this->version, $this->encoding);
        }

        if ($rootElement !== null) {
            $xml->startElement($rootElement);
            $this->createElements($xml, $items);
            $xml->endElement();
        } else {
            $this->createElements($xml, $items);
        }

        return $xml->outputMemory(true);
    }

    /**
     * Extract the parent name declared with '%' symbol e.g. '%parent' => 'name'
     *
     * @example  [
     *      '%parent' => 'country',
     *      'name' => 'Germany',
     *      'capital' => 'Berlin',
     * ];
     *
     * Would produce:
     *
     * <country>
     *      <name>Germany</name>
     *      <capital>Berlin</capital>
     * </country>
     *
     * @param array &$items
     * @throws \UnexpectedValueException
     * @return string|null Parent element name
     */
    protected function extractParent(array &$items): ?string
    {
        $parentName = null;

        foreach (array_keys($items) as $name) {
            if ($name[0] === '%') {
                if ($parentName !== null) {
                    throw new \UnexpectedValueException(
                        sprintf(
                            'Parent already declared: "%s", unexpected "%s"',
                            "$parentName",
                            $name
                        )
                    );
                }
                $parentName = $items[$name];
                unset($items[$name]);
            }
        }

        return $parentName;
    }

    /**
     * @param \XMLWriter $xml
     * @param array $items
     * @throws \UnexpectedValueException
     * @return void
     */
    protected function createElements(\XMLWriter $xml, array $items)
    {
        $parentName = $this->extractParent($items);

        if ($parentName !== null) {
            $xml->startElement($parentName);
        }

        foreach ($items as $name => $value) {
            if (is_string($name)) {
                if ($name[0] === '@') {
                    if (!is_string($value)) {
                        throw new \UnexpectedValueException(
                            sprintf(
                                'Unexpected type "%s", string expected',
                                gettype($value)
                            )
                        );
                    }
                    $xml->writeAttribute(substr($name, 1), $value);
                } else {
                    if (is_array($value)) {
                        $xml->startElement($name);
                        $this->createElements($xml, $value);
                        $xml->endElement();
                    } else {
                        $xml->writeElement($name, $value);
                    }
                }
            } else {
                if (!is_array($value)) {
                    throw new \UnexpectedValueException(
                        sprintf(
                            'Unexpected type "%s", array expected',
                            gettype($value)
                        )
                    );
                }
                $this->createElements($xml, $value);
            }
        }

        if ($parentName !== null) {
            $xml->endElement();
        }
    }
}
