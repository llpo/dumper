<?php

namespace Dumper;

/**
 * Class Dumper
 */
class Dumper
{
    /**
     * Supported native formats
     *
     * @var array
     */
    const OUTPUT_FORMATS = ['xml', 'json', 'csv'];

    /**
     * @var string
     */
    protected $xmlVersion;

    /**
     * @var string
     */
    protected $xmlEncoding;

    /**
     * @var string Root element
     */
    protected $xmlRootElement;

    /**
     * @var string Used only in CSV
     */
    protected $csvDelimiter;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var bool
     */
    protected $verbose;

    /**
     * @var bool
     */
    protected $pprint;

    /**
     * @var string
     */
    protected $outputFormat;

    /**
     * @var string
     */
    protected $fetchQuery;

    /**
     * @var string
     */
    protected $countQuery;

    /**
     * @var array
     */
    protected $pdo;

    /**
     * @var function
     */
    protected $writer;

    /**
     * @var function
     */
    protected $transformer;

    /**
     * @param array $settings
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     * @return void
     */
    public function __construct(array $settings)
    {
        if (empty($settings['pdo'])) {
            throw new \InvalidArgumentException(
                'PDO parameters required'
            );
        }

        if (is_array($settings['pdo'])) {
            if (empty($settings['pdo']['dsn'])) {
                throw new \InvalidArgumentException(
                    'PDO parameter "dsn" missing'
                );
            }
            $this->pdo = [
               'dsn' =>  $settings['pdo']['dsn'],
                'username' => $settings['pdo']['username'] ?? '',
                'password' => $settings['pdo']['password'] ?? '',
                'options' => $settings['pdo']['options'] ?? [],
            ];
        } else {
            if ($settings['pdo'] instanceof \PDO) {
                $this->pdo = $settings['pdo'];
            } else {
                throw new \UnexpectedValuetException(
                    'Unexepceted data type, PDO instance or Array expected'
                );
            }
        }

        $defaultSettings = [
            'limit' => 10000,
            'verbose' => false,
            'pprint' => false,
            'xmlRootElement' => 'items',
            'xmlVersion' => '1.0',
            'xmlEncoding' => 'utf-8',
            'outputFormat' => 'xml',
            'csvDelimiter' => ',',
            'pdo' => null,
        ];

        $settings = array_merge($defaultSettings, $settings);
        foreach ($settings as $name => $value) {
            if (!array_key_exists($name, $defaultSettings)) {
                throw new \UnexpectedValueException(
                    sprintf('Unknown setting name "%s"', $name)
                );
            }
            $method = 'set' .  ucfirst($name);
            if (property_exists($this, $name)) {
                $this->$name = $value;
            } elseif (method_exists($this, $method)) {
                $this->$method($value);
            } else {
                throw new \UnexpectedValueException(
                    sprintf('Property name "%s" doesn\'t exist', $name)
                );
            }
        }

        $this->writer = null;
        $this->transformer = null;
    }

    /**
     * @param string $format
     * @throws \UnexpectedValueException
     * @return self
     */
    public function setOutputFormat(string $format): self
    {
        if (!in_array($format, self::OUTPUT_FORMATS, true)) {
            throw new \UnexpectedValueException(
                sprintf('Unknown format "%s"', $format)
            );
        }

        $this->outputFormat = $format;

        return $this;
    }

    /**
     * @param string $file
     * @throws \UnexpectedValueException
     * @return self
     */
    public function setOutputFile(string $file): self
    {
        if (trim($file) === '' || $file === null) {
            throw new \UnexpectedValueException(
                sprintf('Wrong filename, unexpected empty value')
            );
        }

        return $this;
    }

    /**
     * @param string $sqlQuery
     * @throws \UnexpectedValueException
     * @return self
     */
    public function setFetchQuery(string $sqlQuery): self
    {
        $sqlQuery = trim($sqlQuery);
        if (empty($sqlQuery)) {
            throw new \UnexpectedValueException(
                sprintf('Wrong SQL query, unexpected empty value')
            );
        }

        $this->fetchQuery = $sqlQuery;

        return $this;
    }

    /**
     * @param string $sqlQuery
     * @throws \UnexpectedValueException
     * @return self
     */
    public function setCountQuery(string $sqlQuery): self
    {
        $sqlQuery = trim($sqlQuery);
        if (empty($sqlQuery)) {
            throw new \UnexpectedValueException(
                sprintf('Wrong SQL query, unexpected empty value')
            );
        }

        $this->countQuery = $sqlQuery;

        return $this;
    }

    /**
     * @param function|null $transformer A NULL value sets default transformer
     * @throws \InvalidArgumentException
     * @return self
     */
    public function setTransformer($transformer): self
    {
        if ($transformer !== null && !is_callable($transformer)) {
            throw new \InvalidArgumentException(
                'Paramter "transfomer" is not callable'
            );
        }

        $this->transformer = $transformer;

        return $this;
    }

    /**
     * @param function|null $writer A NULL value sets default writer
     * @throws \InvalidArgumentException
     * @return self
     */
    public function setWriter($writer): self
    {
        if ($writer !== null && !is_callable($writer)) {
            throw new \InvalidArgumentException(
                'Paramter "writer" is not callable'
            );
        }

        $this->writer = $writer;

        return $this;
    }

    /**
     * @param array $payload
     * @return string
     */
    protected function defaultWriter(array $payload): string
    {
        $buffer = '';
        $indent = str_repeat(' ', 4);

        switch ($this->outputFormat) {
            case 'xml':
                $buffer = (new Xmler(null, null, $this->pprint))->make($payload);
                if ($this->pprint) {
                    $buffer = preg_replace('/^/m', $indent, $buffer);
                }
                break;

            case 'json':
                $options = JSON_UNESCAPED_UNICODE;
                $options |= $this->pprint ? JSON_PRETTY_PRINT : 0;
                foreach ($payload as $row) {
                    $json = json_encode($row, $options);
                    if ($this->pprint) {
                        $buffer .= PHP_EOL;
                        $buffer .= preg_replace('/^/m', $indent, $json);
                        $buffer .= ',';
                    } else {
                        $buffer .=  json_encode($row, $options) . ',';
                    }
                }
                break;

            case 'csv':
                foreach ($payload as $row) {
                    $row = array_map(
                        function ($value) {
                            return '"' . mb_ereg_replace('"', '""', $value) . '"';
                        },
                        $row
                    );
                    $buffer .= implode($this->csvDelimiter, $row) . PHP_EOL;
                }
                break;

            default:
                throw new \UnexpectedValueException(
                    sprintf('Unknown output format "%s"', $this->outputFormat)
                );
        }

        return $buffer;
    }

    /**
     * @return string|null
     */
    protected function header(): ?string
    {
        $buffer = null;

        switch ($this->outputFormat) {
            case 'xml':
                $buffer = '<?xml ';
                if ($this->xmlVersion !== null) {
                    $buffer .= 'version="' . $this->xmlVersion . '" ';
                }
                if ($this->xmlEncoding !== null) {
                    $buffer .= 'encoding="' . $this->xmlEncoding . '" ';
                }
                $buffer .= '?>';
                $buffer .= ($this->pprint ? "\n" : '');
                $buffer .= "<{$this->xmlRootElement}>";
                $buffer .= ($this->pprint ? "\n" : '');
                break;

            case 'json':
                $buffer = '[';
                break;

            default:
                $buffer = null;
        }

        return $buffer;
    }

    /**
     * @param int $page Current page
     * @param int $pages Total pages
     * @param array $rows Associative array
     * @return string
     */
    protected function page(int $page, int $pages, array $rows): string
    {
        $values = [];

        if ($this->outputFormat === 'csv' && $page === 0) {
            $values[] = array_keys($rows[0]);
        }

        foreach ($rows as $row) {
            if ($this->transformer !== null) {
                $transformer = $this->transformer;
                $row = $transformer($row);
            }
            $values[] = $row;
        }

        if ($this->writer !== null) {
            $buffer = $this->writer($values);
        } else {
            $buffer = $this->defaultWriter($values);
        }

        // Remove last delimieter character if needed
        if ($page < $pages - 1) {
            return $buffer;
        }

        switch ($this->outputFormat) {
            case 'json':
                $buffer = rtrim($buffer, ',');
                break;
            case 'csv':
                $buffer = rtrim($buffer, PHP_EOL);
                break;
        }

        return $buffer;
    }

    /**
     * @return string|null
     */
    protected function footer(): ?string
    {
        $buffer = '';

        switch ($this->outputFormat) {
            case 'xml':
                $buffer .= "</{$this->xmlRootElement}>";
                break;

            case 'json':
                $buffer .= ($this->pprint ? "\n" : '');
                $buffer .= ']';
                break;

            default:
                $buffer = null;
        }

        return $buffer;
    }

    /**
     * @return \PDO
     */
    protected function dbConnect(): \PDO
    {
        if (!($this->pdo instanceof \PDO)) {
            $connection = new \PDO(
                $this->pdo['dsn'],
                $this->pdo['username'] ?? null,
                $this->pdo['password'] ?? null,
                $this->pdo['options'] ?? null
            );
        } else {
            $connection = $this->pdo;
        }

        $connection->setAttribute(
            \PDO::ATTR_CASE,
            \PDO::CASE_NATURAL
        );
        $connection->setAttribute(
            \PDO::ATTR_DEFAULT_FETCH_MODE,
            \PDO::FETCH_ASSOC
        );
        $connection->setAttribute(
            \PDO::ATTR_ERRMODE,
            \PDO::ERRMODE_EXCEPTION
        );

        return $connection;
    }

    /**
     * @param \PDO $connection
     * @throws \UnexpectedValueException
     * @return int Number of rows counted
     */
    protected function countRows(\PDO $connection): int
    {
        if ($this->countQuery === null) {
            if (empty($this->fetchQuery)) {
                throw new \UnexpectedValueException(
                    sprintf('Wrong SQL query, unexpected empty value')
                );
            }
            $from = preg_split('/FROM/i', $this->fetchQuery);
            $pieces = count($from);
            if ($pieces < 1) {
                throw new \UnexpectedValueException(
                    'Not possible to extract FROM clause, ' .
                    'consider to set a custom query with Dumper::setCountQuery'
                );
            }
            $table = preg_split('/ORDER\s+/i', $from[$pieces - 1])[0];
            $statement = $connection->query(
                "SELECT COUNT(*) AS num FROM $table"
            );
            $count = (int) $statement->fetch(\PDO::FETCH_ASSOC)['num'];
        } else {
            $statement = $connection->query($this->countQuery);
            $result = $statement->fetch(\PDO::FETCH_NUM);
            if (count($result) !== 1 || count($result[0]) !== 1) {
                throw new \UnexpectedValueException(
                    'COUNT query set with Dumper::setCountQuery ' .
                    'must return only one'
                );
            }
            $count = (int) $result[0];
        }

        if ($this->verbose) {
            echo "(i) Total records count {$count}\n";
        }

        return $count;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     *
     * @param string $outputFile
     * @param bool $forceCreate
     * @return integer Total of records dumped
     */
    public function dump(string $outputFile, bool $forceCreate = false): int
    {
        $totalTime = microtime(true);
        $connection = $this->dbConnect();
        $total = $this->countRows($connection);
        $pages = ceil($total / $this->limit);

        if ($this->verbose) {
            echo "(i) Output file {$outputFile}\n",
                "(i) Pages $pages / records $total\n";
        }

        $handle = fopen($outputFile, $forceCreate ? 'w' : 'x');
        if ($handle === false) {
            throw new \Exception(
                sprintf('Unable to create file "%s"', $outputFile)
            );
        }

        $bytes = 0;
        $count = 0;

        $buffer = $this->header();
        if ($buffer !== null) {
            $bytes += fwrite($handle, $buffer);
        }

        for ($page = 0; $page < $pages; $page++) {
            $fetchTime = microtime(true);
            $offset = $page * $this->limit;
            $query = "{$this->fetchQuery} LIMIT {$this->limit} OFFSET $offset";
            $rows = $connection->query($query)->fetchAll();
            $fetchTime = microtime(true) - $fetchTime;
            $bytes += fwrite($handle, $this->page($page, $pages, $rows));
            $count += count($rows);
            if ($this->verbose) {
                printf(
                    "(i) Page %d of %d / Size %s / Fetch %s / Total elapsed %s\n",
                    $page + 1,
                    $pages,
                    $this->formatBytes($bytes),
                    $this->formatSeconds($fetchTime),
                    $this->formatSeconds(microtime(true) - $totalTime)
                );
            }
        }

        $buffer = $this->footer();
        if ($buffer !== null) {
            $bytes += fwrite($handle, $buffer);
        }

        if ($count !== $total) {
            echo "Warning: counts mismatch, rows counted $total, ",
               "rows fetched $count. Revise you SQL query.";
        }

        fclose($handle);

        if ($this->verbose) {
            printf(
                "(i) Total saved: records %d / Size %s / Time: %s\n",
                $count,
                $this->formatBytes($bytes),
                $this->formatSeconds(microtime(true) - $totalTime)
            );
        }

        return $count;
    }

    /**
     * @param int $bytes
     * @param int $precision
     * @return string
     */
    protected function formatBytes(int $bytes, int $precision = 2):  string
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];
        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);
        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . $units[$pow];
    }

    /**
     * @param float $stamp
     * @return string
     */
    protected function formatSeconds(float $stamp):  string
    {
        $minutes = floor(($stamp % 3600) / 60);
        $hours = floor(($stamp % 86400) / 3600);
        $days = floor(($stamp % 2592000) / 86400);
        $seconds = round($stamp - ($minutes * 60), 2);

        $units = [];
        if ($days > 0) {
            $units[] = "$days d.";
        }
        if ($hours > 0) {
            $units[] = "$hours h.";
        }
        if ($minutes > 0) {
            $units[] = "$minutes min.";
        }
        $units[] = "$seconds sec.";

        return implode(' ', $units);
    }

    /**
     *  @param string $name
     *  @throws \DomainException
     *  @return miexed
     */
    public function __get($name)
    {
        if (!isset($this->$name)) {
            throw new \UnexpectedValueException(
                sprintf('Unknown property name "%s"', $name)
            );
        }

        return $this->$name;
    }
}
